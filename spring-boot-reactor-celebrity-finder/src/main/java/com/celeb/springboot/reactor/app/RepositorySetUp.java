package com.celeb.springboot.reactor.app;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.stereotype.Component;

import com.celeb.springboot.reactor.app.models.Person;
import com.celeb.springboot.reactor.app.models.dao.PeopleRepository;

import reactor.core.publisher.Flux;

@Component
public class RepositorySetUp {

	// Simple log for displayig the data 
		private static final Logger log = LoggerFactory.getLogger(RepositorySetUp.class);
		
		@Autowired
		private ReactiveMongoTemplate mongoTemplate; // Simplify mongo operations
		
		@Autowired
		private PeopleRepository repository;
		
		
		
		
	void setUpTeam() {
		 log.info("setUpTeam : cleanning mongo bd");
		//cleanning de bd for new runs
		mongoTemplate.dropCollection("people").subscribe();
		
		 log.info("setUpTeam : get sample people");
		List<String> n = getSamplePeople();
		Person lee = new Person(n.get(8));
		log.info("setUpTeam : creating the Observable from the array "+n);
		Flux<String> names = Flux.fromIterable(n); 
		
		Flux<Person> peopleTeam = setUpPeople(n, lee,   names);	
		log.info("setUpTeam : Attempting to persist people ");
		persistPeople(peopleTeam);
	}

	private void persistPeople(Flux<Person> peopleTeam) {
		//persist data to de BD
		 peopleTeam.flatMap(p ->  {
				return repository.save(p);
				})
			.subscribe(p -> log.info("Person persisted: "+ p.getName() ) , error -> log.error( error.getMessage()));
	}

	private List<String> getSamplePeople() {
		// Array of people for populating the mongo db ,  by default the db is called test 
		List<String> n = new ArrayList<String>();
		n.add("andres guzman");
		n.add("Pedro Fulano");
		n.add("Maria fulana");
		n.add("diego arismendi");
		n.add("juan Sutano");	
		n.add("Jorge gonzo");
		n.add("Michael Black");
		n.add("Nancy Britto");
		n.add("Bruk Willis");
		n.add("Bruce Lee");// celebrity
	
		return n;
	}

	private Flux<Person> setUpPeople(List<String> n, Person lee,  Flux<String> names) {
		// transforming names into the people team , making the graph structure
		Flux<Person> peopleTeam = names.map(name -> {
												Person person = new Person();
												person.setName(name);
												// Making sur that celebs  don't know the people
												if(!person.getName().contains("Bruce ")) {
													 Map<String ,Person> mapOfKnownPeople = new HashMap<String,Person>() ;
													 
													 Random r = new Random();													
													 int randomRelation = r.ints(1, 1, 5).findFirst().getAsInt();// set it uptops to 5
													 													 
													 for(int i = 0; i < randomRelation;  i++ ) {
														 
														 Person known = new Person(n.get(r.ints(1, 0, 8).findFirst().getAsInt()));

														 mapOfKnownPeople.put(known.getName(), known);// pick people randomly
														 
													 }
													 //everyone knows lee and willis
													 mapOfKnownPeople.put(lee.getName(),lee );
												 
													 person.setKnownPeople(Arrays.asList(mapOfKnownPeople.values().toArray()));
													
												}else {
												
													person.setKnownPeople(new ArrayList<Person>());
												}
												return person;												
											});
		return peopleTeam;
	}
}
