package com.learn.springboot.rest.app.payroll;
 

enum Status {

	IN_PROGRESS,
	COMPLETED,
	CANCELLED;
}