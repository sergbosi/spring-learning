package com.bolsadeideas.springboot.webflux.app;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;

import com.bolsadeideas.springboot.webflux.app.models.dao.ProductoDao;
import com.bolsadeideas.springboot.webflux.app.models.documents.Producto;

import reactor.core.publisher.Flux;



@SpringBootApplication
public class SpringBootWebfluxApplication implements CommandLineRunner{

	@Autowired
	private ProductoDao dao; 
	
	@Autowired
	private ReactiveMongoTemplate mongoTemplate;
	
	public static void main(String[] args) {
		SpringApplication.run(SpringBootWebfluxApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		
		
		mongoTemplate.dropCollection("productos").subscribe();
 
		Flux.just(new Producto("TV Panasonic Pantalla LCD", 456.89),
				new Producto("Sony Camara DIgital HD",177.89),
				new Producto("Apple Ipod", 46.98),
				new Producto("Sony notebook", 846.78),
				new Producto("Hewlett Packard Impresora", 200.99),
				new Producto("Hewñett Packard Omen", 2500.99)
				)
		.flatMap(producto -> {
			producto.setCreateAt(new Date());
		return	dao.save(producto);
		})
			.subscribe(producto -> System.out.println("Insert: "+producto.getId()+" "+producto.getNombre()));
	}
}
