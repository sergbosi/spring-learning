package com.celeb.springboot.reactor.app.models.dao;

 
import org.springframework.data.repository.CrudRepository;

import com.celeb.springboot.reactor.app.models.Person;



// Spring Data will generate all the basic CRUD methods
public interface PeopleRepository extends  CrudRepository<Person, String> {

	
}
