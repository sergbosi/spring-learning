package com.celeb.springboot.reactor.app;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
 

import com.celeb.springboot.reactor.app.models.Person;
import com.celeb.springboot.reactor.app.models.dao.PeopleRepository;

import reactor.core.publisher.Flux;


// CommandLineRunner to allow running  the app with run ,  console way
@SpringBootApplication
public class SpringBootReactorApplication implements CommandLineRunner {

	// Simple log for displayig the data 
	private static final Logger log = LoggerFactory.getLogger(SpringBootReactorApplication.class);
	
	
	@Autowired
	private RepositorySetUp repoSetUp; 
	
	@Autowired
	private PeopleRepository repository;
	
	public static void main(String[] args) {
		SpringApplication.run(SpringBootReactorApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		
		// create people and persisit it to a mongo db
		repoSetUp.setUpTeam();
		
		// we could simply query for the relations empty to de DB  but that would be too easy instead well traverse the collection ourselves
		List<Person> peopleTeam = (List<Person>) repository.findAll();
		
		Person celebrity= DivideAndConquer.celebritySearch(peopleTeam, 0);
		
		log.info("Celebrities Found: "+celebrity.getName());
		
		 	
	}

	
	

}
