package com.celeb.springboot.reactor.app.models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

//@Document(collection="people")

@Entity
@Table(name = "people")
public class Person {
	
	@Id
	 @Column(name = "id", unique = true, nullable = false) 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private String id;
	@Column
	private String name;
	 
	/*@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "known_people", referencedColumnName = "id")
	*/
	@OneToMany(mappedBy="id" ,fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Person> knownPeople;
	
	
	
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "parent_id")
    @org.hibernate.annotations.ForeignKey(name = "FK_ITEM_PARENT_ID")    
	private Person parent;
	
 public Person getParent() {
     return this.parent;
 }
  
 public void setParent(Person parent) {
     this.parent = parent;
 }
	
	public Person() {
		
	}
	
	public Person(String name) {
		this.name = name;
	}
	
	public Person(String name , List<Person> knownPeople) {
		this.name = name;
		this.knownPeople=knownPeople;
	}
		
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public List<Person> getKnownPeople() {
		return knownPeople;
	}
	
	public void setKnownPeople(List<? extends Object> list) {
		
		this.knownPeople = (List<Person>) list;
	}
	
	

}
