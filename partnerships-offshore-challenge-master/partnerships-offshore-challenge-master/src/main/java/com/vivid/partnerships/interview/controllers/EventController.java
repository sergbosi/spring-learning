package com.vivid.partnerships.interview.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.vivid.partnerships.interview.model.Event;
import com.vivid.partnerships.interview.model.Venue;
import com.vivid.partnerships.interview.service.EventService;
import com.vivid.partnerships.interview.service.EventServiceI;

import reactor.core.publisher.Flux;
import reactor.util.function.Tuple2;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Stream;

@CrossOrigin(origins = { "/*","http://localhost:4200", "http://interview-vivid.surge.sh/Events" })
@RestController
@RequestMapping("/api")
public class EventController {
    private static final Logger LOGGER = LoggerFactory.getLogger(EventController.class);

    private final EventServiceI eventService;

    @Autowired
    public EventController(final EventServiceI eventService) {
        this.eventService = eventService;
    }

  /* @GetMapping("/events")
    public List<Event> getEvents() {
        List<Event> events = eventService.getEvents();
        LOGGER.info("Returning {} events", events.size());
        return events;
    }*/
    
    
    @GetMapping(value = "/events", produces = MediaType.APPLICATION_JSON_VALUE)
    public Flux<Event> events() {
    	
    	 List<Event> events = eventService.getEvents();
        Flux<Event> eventFlux = Flux.fromStream(events.stream()
            //Stream.generate(
             //   ()->new Event())//System.currentTimeMillis(), LocalDate.now()
            );

        Flux<Long> durationFlux = Flux.interval(Duration.ofSeconds(1));

        return Flux.zip(eventFlux, durationFlux).map(Tuple2::getT1);
    }
    
    @RequestMapping(value = "/events/form", method = RequestMethod.POST)
    public Event saveEvent(@RequestBody Event event) { 
    	 LOGGER.info("saveEvent {} events");
    	Event ev = new Event();
    	Venue ven = new Venue();
    	
    	DateFormat  format = new SimpleDateFormat( "yyyy-MM-dd HH:mm", Locale.US); 
 
    	try {
			ev.setDate(format.parse(format.format(event.getDate())));
	
	    	ev.setName(event.getName());
	    	ven.setName(event.getVenue().getName());
	    	ven.setCity(event.getVenue().getCity());
	    	ven.setState(event.getVenue().getState());
	    	ev.setVenue(ven);
    	
    	 ev = eventService.saveEvent(ev);
    	 
    	} catch (ParseException| RuntimeException e) {
	 
    		LOGGER.error(e.getMessage());
		}
    	
    	 if(ev.getId() != null) {
        	 LOGGER.info("Returning {}  saved event", ev.getId());
         	return ev;
    	 }
    	
    	 return null;
 
    }
}
