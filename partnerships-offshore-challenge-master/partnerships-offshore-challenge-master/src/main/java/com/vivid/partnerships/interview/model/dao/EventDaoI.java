package com.vivid.partnerships.interview.model.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.vivid.partnerships.interview.model.Event;

public interface EventDaoI extends  CrudRepository<Event, Integer>{
	
	@Override
	public List<Event> findAll();
   
}
