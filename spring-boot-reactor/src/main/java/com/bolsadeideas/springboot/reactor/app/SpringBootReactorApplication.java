package com.bolsadeideas.springboot.reactor.app;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.bolsadeideas.springboot.reactor.app.models.Usuario;

import reactor.core.publisher.Flux;

@SpringBootApplication
public class SpringBootReactorApplication implements CommandLineRunner {

		private static final Logger log = LoggerFactory.getLogger(SpringBootReactorApplication.class);
	public static void main(String[] args) {
		SpringApplication.run(SpringBootReactorApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		
		List<String> n = new ArrayList<String>();
		n.add("andres guzman");
		n.add("pedro fulano");
		n.add("Maria fulana");
		n.add("diego arismendi");
		n.add("juan Sutano");
		n.add("Bruce Lee");
		n.add("Bruce Willis");
		n.add("Jorge gonzo");
		
		 Flux<String> nombres =Flux.fromIterable(n); //Flux.just("andres guzman","pedro fulano","Maria fulana","diego arismendi" ,"juan Sutano", "Bruce Lee", "Bruce Willis", " Jorge gonzo");
		 
		Flux<Usuario> usuarios = nombres.map(nombre -> new Usuario(nombre.split(" ")[0],nombre.split(" ")[1]))
				 .filter(usuario ->usuario.getNombre().toLowerCase().equals("bruce"))
				 .doOnNext(elemento -> {
										 if(elemento.getNombre().isEmpty()) {
										  throw new RuntimeException("Los nombres no pueden ser vacios");
										 }
										 System.out.println(elemento.getNombre()+" "+elemento.getApellido());
					 })
				 .map(usuario -> {
					 				usuario.setNombre( usuario.getNombre().toUpperCase());
					 				return usuario;
				 					
				 					});
		 
		 usuarios.subscribe(usuario -> log.info(usuario.getNombre()), error -> log.error(error.getMessage())
				 , new Runnable() {
					
					@Override
					public void run() {
						log.info("Ha terminado la ejecución de Flux (observable) con exito");
					}
				});	
		
	}
}
