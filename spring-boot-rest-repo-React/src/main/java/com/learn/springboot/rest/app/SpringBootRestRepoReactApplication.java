package com.learn.springboot.rest.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootRestRepoReactApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootRestRepoReactApplication.class, args);
	}
}
