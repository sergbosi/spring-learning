package com.bolsadeideas.springboot.app.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bolsadeideas.springboot.app.models.entity.Cliente;
import com.bolsadeideas.springboot.app.models.service.IClienteService;

@RestController
@RequestMapping(value = "/api/clientes" , method = RequestMethod.GET)
public class ClienteRestController {

	@Autowired
	private IClienteService clienteService;

	
	/*REST API*/
	@RequestMapping(value = "/listar" , method = RequestMethod.GET)
	public List<Cliente> listar() {
		
		return clienteService.findAll();
		
	}
	
}
