package com.bolsadeideas.springboot.app.auth.service;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import org.springframework.util.Base64Utils;

import com.bolsadeideas.springboot.app.auth.filter.MixinSimpleGrantedAuthority;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;


@Component
public class JwtServiceImpl implements JwtService {

	public static final String BEARER = "Bearer ";
	public static final String AUTHORITIES = "authorities";
	public static final String SECRET = Base64Utils.encodeToString("Alguna.Clave.Secreta.123456".getBytes());
	public static final String AUTHORIZATION = "Authorization";
	
	@Override
	public String create(Authentication authentication) throws JsonProcessingException {
		 
		String username = authentication.getName();
		 Collection<? extends GrantedAuthority> roles= authentication.getAuthorities();
		 
		 Claims claims = Jwts.claims();
		 claims.put(AUTHORITIES,new ObjectMapper().writeValueAsString(roles));
		
		String token = Jwts.builder()
		.setSubject(username)
		.signWith(SignatureAlgorithm.HS512, SECRET.getBytes())
		.setIssuedAt(new Date())
		.setExpiration(new Date(System.currentTimeMillis() + 3600000L))
		.setClaims(claims)
		.compact();
		
		
		return token;
	}

	@Override
	public boolean validate(String token) {
		boolean validoToken;		 

		try {			
			getClaims(token);
			validoToken= true;
		}
		catch (JwtException | IllegalArgumentException failed) {
			validoToken = false;
		}
		
		return validoToken;
	}

	@Override
	public Claims getClaims(String token) {
		Claims claim= Jwts.parser()
				.setSigningKey(SECRET.getBytes())
				.parseClaimsJws(resolve(token)).getBody();
	 
		return claim;
	}

	@Override
	public String getUsername(String token) {
		 return getClaims(token).getSubject();

	}

	@Override
	public Collection<? extends GrantedAuthority> getRoles(String token) throws JsonParseException, JsonMappingException, IOException {
		Object roles = getClaims(token).get(AUTHORITIES);
		Collection<? extends GrantedAuthority> authorities = Arrays.asList(new ObjectMapper().addMixIn(SimpleGrantedAuthority.class, MixinSimpleGrantedAuthority.class)
																					.readValue(roles.toString().getBytes()
																		, SimpleGrantedAuthority[].class));
		

		return authorities;
	}

	@Override
	public String resolve(String token) {
	 
		return token.replace(BEARER,"");
	}

}
