package com.bolsadeideas.springboot.app.view.xml;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.bolsadeideas.springboot.app.models.entity.Cliente;

@XmlRootElement(name="clientes")
public class ClienteList {

	@XmlElement(name="cliente")
	public List<Cliente> clienteList;

	public ClienteList() {
		clienteList = new ArrayList<Cliente>();
	}
	
	public ClienteList( List<Cliente> c) {
		this.clienteList = c;
	}

	public List<Cliente> getClientes() {
		return clienteList;
	}

	public void setClientes(List<Cliente> clientes) {
		this.clienteList = clientes;
	}
	
	
}
