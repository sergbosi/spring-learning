package com.bolsadeideas.springboot.app.view.json;

import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import com.bolsadeideas.springboot.app.models.entity.Cliente;
import com.bolsadeideas.springboot.app.view.xml.ClienteList;

@Component("listar.json")
public class ClienteListJsonView extends MappingJackson2JsonView {

	@Override
	protected Object filterModel(Map<String, Object> model) {

		
		
		Page<Cliente> clientes = (Page<Cliente>) model.get("clientes");
		model.put("clienteList", new ClienteList(clientes.getContent()));
		model.remove("titulo");
		model.remove("page");
		model.remove("clientes");
		
		return super.filterModel(model);
	}

}
